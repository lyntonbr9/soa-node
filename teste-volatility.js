declare	lower;
declare	hide_on_intraday; input	days_back	=	252;	
#	implied volatility
#	using	proxies	for	futures
def	df	=	if	(GetSymbol()	==	"/ES")	then	close("VIX")	/	100
else	if	(GetSymbol()	==	"/CL")	then	close("OIV")	/	100	
else	if	(GetSymbol()	==	"/GC")	then	close("GVX")	/	100	
else	if	(GetSymbol()	==	"/SI")	then	close("VXSLV")	/	100	
else	if	(GetSymbol()	==	"/NQ")	then	close("VXN")	/	100	
else	if	(GetSymbol()	==	"/TF")	then	close("RVX")	/	100	
else	if	(GetSymbol()	==	"/YM")	then	close("VXD")	/	100	
else	if	(GetSymbol()	==	"/6E")	then	close("EVZ")	/	100	
else	if	(GetSymbol()	==	"/ZN")	then	close("VXTYN")	/	100	
else	imp_volatility();
def	df1	=	if	!IsNaN(df)	then	df	else	df[-1];
#	display	regular	implied	volatility
#	calculate	the	IV	rank
#	calculate	the	IV	rank
def	low_over_timespan	=	Lowest(df1,	days_back);
def	high_over_timespan	=	Highest(df1,	days_back);
def	iv_rank	=	Round(	(df1	- low_over_timespan)	/	(high_over_timespan	- low_over_timespan)	*	100.0,	
0);
plot	IVRank	=	iv_rank;
IVRank.SetDefaultColor(Color.GREEN);
AddLabel(yes,	"IV	Rank:	"	+	iv_rank	+	"	(IV	in	comparison	to	its	yearly	high	and	low	IV)",	Color.GREEN);
#	calculate	the	IV	percentile
#	how	many	times	over	the	past	year,	has	IV	been	below	the	current	IV
def	counts_below	=	fold	i	=	1	to	days_back	+	1	with	count	=	0	
do	
		if	df1[0]	>	df1[i]	then	count	+	1
		else count;
def	iv_percentile	=	Round(counts_below	/	days_back	*	100.0,	0);
plot	IVPercentile	=	iv_percentile;
IVPercentile.SetDefaultColor(Color.YELLOW);
AddLabel(yes,	"IV	Percentile:	"	+	iv_percentile	+	"	("	+	iv_percentile	+	"%	of	days	or	"	+	counts_below	+	
"	days	out	of	"	+	days_back	+	"	days	were	below	the	current	IV)",	Color.YELLOW);